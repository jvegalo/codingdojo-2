CODING DOJO 2 - Layered Architecture

The following application was developed to register a user in a database, using JSF as the front-end technology and MongoDB as Database solution.

This Repository contains the 4 projects (layers) used to develop the complete layered architecture. Each git branch contains a different layer. For example:

The branch named WebBranch contains the web layer project (Front-end).

Members:
- Jose Miguel Vega 
- Jose Miguel Blanco
- Sara Giraldo
- Jose Alejandro Cortes
- Andres Franco

